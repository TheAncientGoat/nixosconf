# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let   antigen = pkgs.fetchgit {
      url = "https://github.com/zsh-users/antigen";
      rev = "1d212d149d039cc8d7fdf90c016be6596b0d2f6b";
      sha256 = "1c7ipgs8gvxng3638bipcj8c1s1dn3bb97j8c73piv2xgk42aqb9";
      fetchSubmodules = true;
};
in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  time.timeZone = "Asia/Singapore";

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    curl
    git
    zip
    google-chrome-beta
    vim
    emacs
    filelight
    nodejs-7_x
    keepassx-community
    elixir
    ark
gnumake
gcc
inotify-tools
  ];

  # List services that you want to enable:
  #
  services.postgresql.enable = true;
  services.postgresql.package = pkgs.postgresql96;
  services.postgresql.dataDir = "/var/db/postgresql/9.6";
  services.postgresql.authentication = pkgs.lib.mkForce ''
    # Generated file; do not edit!
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             all                                     trust
    host    all             all             127.0.0.1/32            trust
    host    all             all             localhost            trust
    host    all             all             ::1/128                 trust
    '';


  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    interactiveShellInit = ''
      source ${antigen}/antigen.zsh
      # Load the oh-my-zsh's library.
      antigen use oh-my-zsh
      # Bundles from the default repo (robbyrussell's oh-my-zsh).
      antigen bundle git
      antigen bundle git-extras
      # Syntax highlighting bundle.
      antigen bundle zsh-users/zsh-syntax-highlighting
      # Load the theme.
      antigen theme wedisagree
      # Tell antigen that you're done.
      antigen apply     
    '';
  };
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.ryan = {
     isNormalUser = true;
     home = "/home/ryan";
     extraGroups = [ "wheel" ];
     uid = 1000;
     shell = pkgs.zsh;
  };
  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "17.03";

}
